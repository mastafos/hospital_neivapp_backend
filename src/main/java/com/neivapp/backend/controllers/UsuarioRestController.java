package com.neivapp.backend.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import com.neivapp.backend.models.entity.Examenes;
//import com.neivapp.backend.models.entity.Roles;
import com.neivapp.backend.models.entity.Usuarios;
import com.neivapp.backend.models.services.UsuarioService;

@CrossOrigin(origins= {"http://localhost:3000"})
@RestController
@RequestMapping("/api")
public class UsuarioRestController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	
	//@Autowired
	//private ExamenService examenService;
	
	@GetMapping("/usuarios")
	public List<Usuarios> index(){
		return usuarioService.findAll();
	}
	
	@GetMapping("/usuariobyid/{id}")
	public ResponseEntity<?> showId(@PathVariable Long id) {
		
		Usuarios usuario = usuarioService.selectById(id);
		return new ResponseEntity<Usuarios>(usuario, HttpStatus.OK);
	}
	
	@GetMapping("/usuario/{ndivalue}")
	public ResponseEntity<?> showNdivalue(@PathVariable Long ndivalue) {
		Usuarios usuario = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			usuario = usuarioService.selectByNdivalue(ndivalue);			
		}
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		if(usuario == null) {
			response.put("Mensaje","El usuario con número de documento: ".concat(Long.toString(ndivalue)).concat(", no existe en la base de datos "));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND); 
		}
		
		return new ResponseEntity<Usuarios>(usuario, HttpStatus.OK);
	}
	
	@PostMapping("/new-user")
	@ResponseStatus(HttpStatus.CREATED)
	public  ResponseEntity<?> createUser(@RequestBody Usuarios newUser) {
		
		Usuarios nuevoUsuario = null;
		
		Map<String, Object> response = new HashMap<>();
		try {
			
			nuevoUsuario = usuarioService.insert(newUser);	
		}
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","El usuario se ha creado con éxito");
		response.put("usuario", nuevoUsuario);
		return new ResponseEntity<Usuarios>(nuevoUsuario, HttpStatus.CREATED);
	}
	
	@PostMapping("/update-usuario/{ndivalue}")
	@ResponseStatus(HttpStatus.OK)
	public Usuarios updateUser(@RequestBody Usuarios userUpdated, @PathVariable Long ndivalue) {
		Usuarios usuarioActual = usuarioService.selectByNdivalue(ndivalue);
		if(usuarioActual.getNdivalue() != null) {
			return usuarioService.update(userUpdated);	
		}
		else {return null;}
	}
	
	@GetMapping("/delete-usuario/{ndivalue}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("ndivalue") Long ndivalue) {
		Usuarios usuario = usuarioService.selectByNdivalue(ndivalue);
		
		//for(Roles rol : usuario.getRoles()) {
			//if(rol.getRol().equals("ADMIN")) {
			if(usuario.getRoles().equals("ADMIN")) {
				//Examenes examen = 
				usuarioService.delete(usuario);
			}
		//}
	}
	
	

	
}
