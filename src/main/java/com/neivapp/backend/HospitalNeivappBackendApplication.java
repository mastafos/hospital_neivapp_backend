package com.neivapp.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalNeivappBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalNeivappBackendApplication.class, args);
	}

}
