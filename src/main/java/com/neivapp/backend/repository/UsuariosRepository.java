package com.neivapp.backend.repository;


import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neivapp.backend.models.entity.Usuarios;


@Repository("usuarioRepositorio")
public interface UsuariosRepository extends JpaRepository<Usuarios, Serializable> {
	
	public abstract Usuarios findByNdivalue(Long nidValue);
	public abstract List<Usuarios> findByFechaRegistroNotNull();
	public abstract Usuarios findById(Long id);

}
