package com.neivapp.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neivapp.backend.models.entity.Roles;

@Repository("rolesRepositorio")
public interface RolesRepository extends JpaRepository<Roles, Long> {
	public abstract Roles findByRol(String name);
}
