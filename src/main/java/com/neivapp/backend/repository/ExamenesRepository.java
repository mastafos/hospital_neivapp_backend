package com.neivapp.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neivapp.backend.models.entity.Examenes;

@Repository("examenesRepositorio")
public interface ExamenesRepository extends JpaRepository<Examenes, Long> {
	 
	 public abstract List<Examenes> findByDoneOrderByIdDesc(boolean done);
	 
	 public abstract List<Examenes> findByNuorden(int number_orden);
	 
}