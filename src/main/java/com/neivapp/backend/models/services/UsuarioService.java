package com.neivapp.backend.models.services;

import java.util.List;

import com.neivapp.backend.models.entity.Usuarios;
import com.neivapp.backend.models.entity.ResetPassword;

public interface UsuarioService {
	
	
	public List<Usuarios> findAll();
	
	
	/**
	 * select Usuarios by identification number
	 * 
	 * @param ndivalue
	 * @return Usuarios object
	 */
	public Usuarios selectByNdivalue(Long ndivalue);

	/**
	 * select Usuarios by id
	 * 
	 * @param id
	 * @return Usuarios object
	 */
	public Usuarios selectById(Long id);

	/**
	 * insert Usuarios in the database
	 * 
	 * @param Usuarios
	 * @return 
	 * @return 
	 */
	Usuarios insert(Usuarios Usuarios);

	/**
	 * update Usuarios in the database
	 * 
	 * @param Usuarios
	 */
	Usuarios update(Usuarios Usuarios);

	/**
	 * update password in the database
	 * 
	 * @param Usuarios
	 */
	public void updatePassword(Usuarios Usuarios);

	/**
	 * delete Usuarios in the database
	 * 
	 * @param Usuarios
	 */
	public void delete(Usuarios Usuarios);

	/**
	 * find Usuarios in the database
	 * 
	 * @param ndivalue
	 * @return true if exist Usuarios
	 */
	public boolean findIsRegistrated(String ndivalue);


	/**
	 * insert token for to reset password in the database
	 * 
	 * @param Usuarios
	 * @param token
	 */
	public void createPasswordResetTokenForUsuarios(Usuarios Usuarios, String token);

	/**
	 * select token object by token data in the database
	 * 
	 * @param token
	 * @return PasswordResetToken object
	 */
	public ResetPassword selectByToken(String token);

	/**
	 * delete token for reset password in the database
	 * 
	 * @param ResetPassword
	 */
	public void deletePasswordResetToken(ResetPassword passwordResetToken);

	/**
	 * delete token for reset password by Usuarios
	 * 
	 * @param usuarios_id
	 */
	public void deletePasswordResetTokenByUsuarios(Integer usuarios_id);

	/**
	 * delete token expired
	 */
	public void deleteExpiredPasswordResetToken();
	
	/**
	 * @param id
	 * @param token
	 */
	public String validatePasswordResetToken(Integer id, String token);
	
}
