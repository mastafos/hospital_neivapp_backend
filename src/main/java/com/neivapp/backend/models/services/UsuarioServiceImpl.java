package com.neivapp.backend.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.neivapp.backend.models.entity.ResetPassword;
import com.neivapp.backend.models.entity.Roles;
import com.neivapp.backend.models.entity.Usuarios;
import com.neivapp.backend.repository.RolesRepository;
import com.neivapp.backend.repository.UsuariosRepository;

@Service("usuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService{

	
	@Autowired
	@Qualifier("usuarioRepositorio")
	private UsuariosRepository usuarioRepo;
	
	@Autowired
	@Qualifier("rolesRepositorio")
	private RolesRepository rolesRepo;
	
	//@Autowired
	//private PasswordEncoder passwordEncoder;

	
	@Override
	@Transactional(readOnly=true)
	public List<Usuarios> findAll() {
		return (List<Usuarios>) usuarioRepo.findAll();
	}

	@Override
	public Usuarios selectByNdivalue(Long ndivalue) {
		Usuarios usuario = usuarioRepo.findByNdivalue(ndivalue);
		
		
		//usuario.agregarRol();
		//if (usuario.getNdivalue() != null) {
			//return usuario;
		//}
		return usuario;
	}

	@Override
	public Usuarios selectById(Long id) {
		Usuarios usuario = usuarioRepo.findById(id);
		if (usuario.getId() != null) {
			return usuario;
		}
		return null;
	}

	@Override
	public Usuarios insert(Usuarios usuarios) {
		//Usuarios usuario = usuarioRepo.findById(usuarios.getId());
		
		//Roles rol = new Roles();
		//rol.setRol(usuarios.getUserrole());
		//usuarios.setRoles(rol);
		//usuarios.getRoles()
	
		/**
		 * 
			if(usuario.getId() == null) {
				System.out.println("Ingresa un usuario nuevo");
				return null;
			}
		 */
		//44 Optional<Roles> rolUsuario = rolesRepo.findById(usuarios.getUserrole());
		System.out.println("Ingresa un usuario nuevo");
		//LA REGION TAMBIEN ES UN MANYTOMANY, VAYA Y MIRE EL HTML DEL FORMULARIO DONDE SETEAN REGION
		//usuarios.agregarRol(rolUsuario);//(rolUsuario.get());
		//rolesRepo.save(rol);
		return usuarioRepo.save(usuarios);			
	}

	@Override
	public Usuarios update(Usuarios usuario) {
		
		Usuarios entity = usuarioRepo.findById(usuario.getId());
				//usuarioRepo.findById(usuario.getId());
		if (entity.getId() != null) {
			if (!usuario.getPassword().equals(entity.getPassword())) {
				//usuario.setPassword(passwordEncoder.encode(usuario));
			}
			//userDao.update(user);
			return usuarioRepo.save(usuario);
			/**
			 * 
			usuario.eliminarRol(usuario.getRoles());
			// remove old roles
			Set<Role> roles = roleDao.selectByUser(user.getId());
			for (Role role : roles) {
				roleDao.removeUserRole(user.getId(), role.getId());
			}

			// set new roles
			for (Role role : user.getRoles()) {
				roleDao.setUserRole(user.getId(), role.getId());
			}
			 */
		}
			return null;
		
	}

	@Override
	public void updatePassword(Usuarios Usuarios) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Usuarios usuarios) {
		usuarioRepo.delete(usuarios);
	}

	@Override
	public boolean findIsRegistrated(String ndivalue) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createPasswordResetTokenForUsuarios(Usuarios Usuarios, String token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ResetPassword selectByToken(String token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deletePasswordResetToken(ResetPassword passwordResetToken) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePasswordResetTokenByUsuarios(Integer usuarios_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteExpiredPasswordResetToken() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String validatePasswordResetToken(Integer id, String token) {
		// TODO Auto-generated method stub
		return null;
	}

}
