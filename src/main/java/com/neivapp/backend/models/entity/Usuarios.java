package com.neivapp.backend.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Entity
@Table(name = "usuarios")
public class Usuarios implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(min = 4, max = 12, message = "el tamaño tiene que estar entre 4 y 12")
	@Column(nullable=false, length = 60)
	private String password;
	
	private String ndi;
	private Long ndivalue;
	private String name;
	private String lastname;
	private String gender;

	@Column(name = "birthdate")
	@Temporal(TemporalType.DATE)
	private Date birthDate;
	
	private String address;
	private String telmobile;
	private String telhome;
	private String telwork;
	
	//@Column(nullable=false, unique=true)
	@NotEmpty(message="El campo no puede estar vacío")
	//@Email(message="Formato de correo incorrecto")
	private String email;
	
	private String maritalStatus;
	private String contact;
	private String communication;
	private String managingOrganization;
	private String bloodtype;
	private String userrole;
	
	@Column(name="fecha_registro")
	private Date  fechaRegistro;
	
	@PrePersist()
	public void prePersist() {
		this.fechaRegistro = new Date();
	}


	private String practitionerRole;

	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(
			name="UsuarioRol", 
			joinColumns=@JoinColumn(name = "id_usuario", referencedColumnName="id"), 
			inverseJoinColumns = @JoinColumn(name = "id_role",referencedColumnName="id"),
			uniqueConstraints= {@UniqueConstraint(columnNames= {"id_usuario","id_role"})}
	)
	private List<Roles> roles;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy="usuarios", cascade=CascadeType.ALL)
	@JsonIgnoreProperties(value={"examenes", "hibernateLazyInitializer", "handler"}, allowSetters=true)
	//@JoinTable(name = "UsuarioExamen", joinColumns = @JoinColumn(name = "idUsuario"), inverseJoinColumns = @JoinColumn(name = "idExamen"))
	private List<Examenes> examenes;
	
	//caso2
	public Usuarios() {
		this.examenes = new ArrayList<>();
	}

	public void agregarRol(Roles tempRol) {
		if(roles == null) {
			roles = new LinkedList<Roles>();
		}
		roles.add(tempRol);
	}
	
	public void eliminarRol(Roles tempRol) {
		if(roles == null) {
			roles = new LinkedList<Roles>();
		}
		roles.remove(tempRol);
	}

	public void agregarExamen(Examenes examen) {
		if(examenes == null) {
			examenes = new LinkedList<Examenes>();
		}
		examenes.add(examen);
	}
	
	public void eliminarExamen(Examenes tempExam) {
		if(examenes == null) {
			examenes = new LinkedList<Examenes>();
		}
		examenes.remove(tempExam);
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNdi() {
		return ndi;
	}
	public void setNdi(String ndi) {
		this.ndi = ndi;
	}
	public Long getNdivalue() {
		return ndivalue;
	}
	public void setNdivalue(Long ndivalue) {
		this.ndivalue = ndivalue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String last_name) {
		this.lastname = last_name;
	} 
	 public String getUserrole() {
		return userrole;
	}
	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelmobile() {
		return telmobile;
	}
	public void setTelmobile(String telmobile) {
		this.telmobile = telmobile;
	}
	public String getTelhome() {
		return telhome;
	}
	public void setTelhome(String telhome) {
		this.telhome = telhome;
	}
	public String getTelwork() {
		return telwork;
	}
	public void setTelwork(String telwork) {
		this.telwork = telwork;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getCommunication() {
		return communication;
	}
	public void setCommunication(String communication) {
		this.communication = communication;
	}
	public String getManagingOrganization() {
		return managingOrganization;
	}
	public void setManagingOrganization(String managingOrganization) {
		this.managingOrganization = managingOrganization;
	}
	public String getBloodtype() {
		return bloodtype;
	}
	public void setBloodtype(String bloodtype) {
		this.bloodtype = bloodtype;
	}
	public String getPractitionerRole() {
		return practitionerRole;
	}
	public void setPractitionerRole(String practitionerRole) {
		this.practitionerRole = practitionerRole;
	}
	
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistroExamen) {
		this.fechaRegistro = fechaRegistroExamen;
	}

	public List<Roles> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}
	
	public List<Examenes> getExamenes() {
		return examenes;
	}
	public void setExamenes(List<Examenes> examenes) {
		this.examenes = examenes;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
