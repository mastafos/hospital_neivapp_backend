package com.neivapp.backend.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
/**
import java.util.List;
import java.util.LinkedList;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
 * 
 */
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="examenes")
public class Examenes implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer nuorden;
	private int codigoExamen;
	private String masInfoExamen;//url
	private String resumeExamen; //Prostático específico Antígeno: Concentración de masa: Punto temporal: Suero o Plasma
	//private String displaySubject;
	private String pacienteId;
	//private String referenceSubject;
	private String nombreExamen;
	private String valorMinimo;
	private String valorMaximo;
	private String valorObtenido;
	private String unidadDeMedida;
	private String comentarioLaboratorista;
	private String nombreEspecialista;
	private String comentarioEspecialista;
	
	@Column(name="fecha_registro_examen")
	@Temporal(TemporalType.DATE)
	private Date fechaRegistroExamen;
	
	@PrePersist()
	public void prePersist() {
		this.fechaRegistroExamen = new Date();
	}
	//private String issued;
	
	private boolean done;
	private String interpretacion;
	
	@JsonIgnoreProperties(value={"examenes", "hibernateLazyInitializer", "handler"}, allowSetters=true)
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name = "UsuarioExamen", joinColumns = @JoinColumn(name = "idExamen"), inverseJoinColumns = @JoinColumn(name = "idUsuario"))
	private List<Usuarios> usuarios;
	
	
	
	public void agregarUsuario(Usuarios usuario) {
		if(usuarios == null) {
			usuarios = new LinkedList<Usuarios>();
		}
		usuarios.add(usuario);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getNuorden() {
		return nuorden;
	}
	public void setNuorden(Integer nuorden) {
		this.nuorden = nuorden;
	}
	public int getCodigoExamen() {
		return codigoExamen;
	}
	public void setCodigoExamen(int codigoExamen) {
		this.codigoExamen = codigoExamen;
	}
	public String getMasInfoExamen() {
		return masInfoExamen;
	}
	public void setMasInfoExamen(String masInfoExamen) {
		this.masInfoExamen = masInfoExamen;
	}
	public String getResumeExamen() {
		return resumeExamen;
	}
	public void setResumeExamen(String resumeExamen) {
		this.resumeExamen = resumeExamen;
	}
	public String getPacienteId() {
		return pacienteId;
	}
	public void setPacienteId(String pacienteId) {
		this.pacienteId = pacienteId;
	}
	public String getNombreExamen() {
		return nombreExamen;
	}
	public void setNombreExamen(String nombreExamen) {
		this.nombreExamen = nombreExamen;
	}
	public String getValorMinimo() {
		return valorMinimo;
	}
	public void setValorMinimo(String valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	public String getValorMaximo() {
		return valorMaximo;
	}
	public void setValorMaximo(String valorMaximo) {
		this.valorMaximo = valorMaximo;
	}
	public String getValorObtenido() {
		return valorObtenido;
	}
	public void setValorObtenido(String valorObtenido) {
		this.valorObtenido = valorObtenido;
	}
	public String getUnidadDeDedida() {
		return unidadDeMedida;
	}
	public void setUnidadDeDedida(String unidadDeDedida) {
		this.unidadDeMedida = unidadDeDedida;
	}
	public String getComentarioLaboratorista() {
		return comentarioLaboratorista;
	}
	public void setComentarioLaboratorista(String comentarioLaboratorista) {
		this.comentarioLaboratorista = comentarioLaboratorista;
	}
	public String getNombreEspecialista() {
		return nombreEspecialista;
	}
	public void setNombreEspecialista(String nombreEspecialista) {
		this.nombreEspecialista = nombreEspecialista;
	}
	public String getComentarioEspecialista() {
		return comentarioEspecialista;
	}
	public void setComentarioEspecialista(String comentarioEspecialista) {
		this.comentarioEspecialista = comentarioEspecialista;
	}
	public boolean isDone() {
		return done;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	public String getInterpretacion() {
		return interpretacion;
	}
	public void setInterpretacion(String interpretacion) {
		this.interpretacion = interpretacion;
	}

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	public List<Usuarios> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuarios> usuarios) {
		this.usuarios = usuarios;
	}
	 * @return
	 */
	
	
	/**
	 * if (exam.getValue() != null) {
			float value = Float.parseFloat(exam.getValue());
			float low = Float.parseFloat(exam.getLow());
			float high = Float.parseFloat(exam.getHigh());
			String code = null;
			if (value > high) {
				code = "H";
			} else if (value < low) {
				code = "L";
			} else {
				code = "N";
			}
			setInterpretation(code);
		}
	 */
	
}
