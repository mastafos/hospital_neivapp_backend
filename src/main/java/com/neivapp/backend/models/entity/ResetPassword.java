package com.neivapp.backend.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//import com.spring.medical.model.User;

@Entity
@Table(name="reset_password")
public class ResetPassword  implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String token;
	//private User user;
	private Date expiryDate;
	
	/**
		public PasswordResetToken() {
	
		}
	
		public PasswordResetToken(User user, String token) {
			this.user = user;
			this.token = token;
		}
	 * 
	 */
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
